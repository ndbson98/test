function randomly(tree, value) {
  if (!tree) return { value };

  const { left, right, ...rest } = tree;
  const shouldGoLeft = Math.random() >= 0.5;
  if (!left || shouldGoLeft)
    return {
      ...rest,
      left: randomly(left, value),
      right,
    };

  return {
    ...rest,
    left,
    right: randomly(right, value),
  };
}

function addLevelOrder(tree, value) {
  if (!tree) return { value };
  let queue = [],
    values = [];
  queue.push(tree);
  while (queue.length > 0) {
    let len = queue.length;
    let level = [];
    for (let i = 0; i < len; i++) {
      let node = queue.shift();
      console.log('node: ', node);
      level.push(node.value);
      if (node.left) {
        queue.push(node.left);
      } else {
        node.left = { value };
        break;
      }
      if (node.right) {
        queue.push(node.right);
      } else {
        node.right = { value };
        break;
      }
    }
    values.push(level);
  }
  // console.log('check: ', values);
  console.log('tree: ', tree);
  // return values;
  return { ...tree };
}

function levelOrder(tree, value) {
  //TODO: Task 1
  return addLevelOrder(tree, value);
}

function addBinaryTree(tree, value) {
  if (!tree) return { value };
  const { left, right } = tree;
  if (value === tree.value) {
    // nếu tồn tại phần tử trong cây thì bỏ qua
    return tree;
  }
  if (value < tree.value) {
    // ktra giá trị input nhỏ hơn root thì cho qua trái và ngược lại
    if (!left) {
      // ktra nếu trỏ trái ko tồn tại thì thêm node
      tree.left = { value };
    } else {
      addBinaryTree(left, value); // lặp lại với input là nhánh con
    }
  } else {
    if (!right) {
      tree.right = { value };
    } else {
      addBinaryTree(right, value);
    }
  }
  return { ...tree };
}

function binarySearchTree(tree, value) {
  //TODO: Task 2
  return addBinaryTree(tree, value);
}

export { randomly, levelOrder, binarySearchTree };
