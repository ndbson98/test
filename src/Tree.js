import React from 'react';
import PropTypes from 'prop-types';
import { Intent, Tag } from '@blueprintjs/core';
import _ from 'lodash';

import NumberInput from './NumberInput';

function Tree({ tree, depth, intent, onRemove }) {
  if (!tree || !tree.value) return null;
  const { value, left, right } = tree;

  return (
    <div style={{ marginTop: 5 }}>
      <span>
        {depth ? `(${depth})${_.repeat('-', depth)}` : ''}
        <Tag
          minimal
          intent={intent}
          round
          onRemove={() => {
            //TODO: Task 3
            onRemove(tree);
          }}
        >
          <NumberInput value={value} />
        </Tag>
      </span>
      <Tree
        tree={left}
        depth={depth + 1}
        intent={Intent.PRIMARY}
        onRemove={onRemove}
      />
      <Tree
        tree={right}
        depth={depth + 1}
        intent={Intent.SUCCESS}
        onRemove={onRemove}
      />
    </div>
  );
}

Tree.propTypes = {
  intent: PropTypes.string,
  tree: PropTypes.shape({
    value: PropTypes.number.isRequired,
    left: PropTypes.object,
    right: PropTypes.object,
  }),
  depth: PropTypes.number.isRequired,
};

Tree.defaultProps = {
  intent: Intent.DANGER,
  tree: undefined,
};

export default React.memo(Tree); //TASK 4
