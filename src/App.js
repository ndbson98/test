import React, { useState, useEffect, useCallback } from 'react';
import {
  Intent,
  ControlGroup,
  Button,
  RadioGroup,
  Radio,
} from '@blueprintjs/core';

import NumberInput from './NumberInput';
import Tree from './Tree';
import { randomly, levelOrder, binarySearchTree } from './tasks';

function App() {
  const [value, setValue] = useState();
  const [option, setOption] = useState('randomly');
  const [tree, setTree] = useState();

  useEffect(() => setTree(), [option]);
  //TASK 4 handle performance
  const setInput = useCallback((val) => {
    setValue(val);
  }, []);

  //TASK 4 handle performance
  const onRemove = useCallback(
    //TASK 3
    (note) => {
      const deleteNode = (root, key) => {
        if (!root) return null;
        if (key < root.value) {
          root.left = deleteNode(root.left, key);
        } else if (key > root.value) {
          root.right = deleteNode(root.right, key);
        } else {
          if (!root.left && !root.right) return null; // ko tồn tại 2 node con
          else if (!root.left && root.right)
            return root.right; // tồn tại 1 node con
          else if (root.left && !root.right) return root.left; // tồn tại 1 node con

          // node có 2 con

          root.value = findLeftTree(root.right).value;
          // tìm phần tử nhỏ nhất bên nhánh phải
          // rồi gán giá trị nó vào node cần xoá
          root.right = deleteNode(root.right, root.value); // xoá node đã gán
        }
        return root;
      };
      let tempt = deleteNode(tree, note.value);
      setTree({ ...tempt });
    },
    [tree]
  );

  const findLeftTree = (root) => {
    // tìm min trong 1 cây.
    if (!root) return null;
    var leftMostNode = root;
    while (leftMostNode.left) {
      leftMostNode = leftMostNode.left;
    }
    return leftMostNode;
  };

  const addNode = () => {
    setTree((prevTree) => {
      switch (option) {
        case 'level':
          setInput(NaN);
          return levelOrder(prevTree, value);
        case 'bst':
          setInput(NaN);
          return binarySearchTree(prevTree, value);
        default:
          setInput(NaN);
          return randomly(prevTree, value);
      }
    });
  };
  return (
    <div style={{ margin: 'auto', width: '80%' }}>
      <RadioGroup
        style={{ marginTop: 10 }}
        inline
        label="Options"
        onChange={(e) => setOption(e.target.value)}
        selectedValue={option}
      >
        <Radio label="Randomly" value="randomly" />
        <Radio label="Level order" value="level" />
        <Radio label="Binary Search Tree" value="bst" />
      </RadioGroup>
      <ControlGroup style={{ marginTop: 10 }}>
        <NumberInput
          inline
          label="Input number:"
          value={value}
          onChange={setInput}
        />
        <div style={{ height: 30, marginLeft: 5 }}>
          <Button
            intent={Intent.PRIMARY}
            text="Add"
            disabled={value ? false : true}
            onClick={addNode}
          />
          <Button
            intent={Intent.DANGER}
            text="Reset"
            onClick={() => {
              setTree();
              setInput(NaN);
            }}
          />
        </div>
      </ControlGroup>
      <Tree tree={tree} depth={0} onRemove={onRemove} />
    </div>
  );
}

export default App;
