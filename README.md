# test

# what have you done, and not done?

Task1 chưa hoàn thành.
Task 2 done.
Task 3 done.
Task 4 done.
Task 5 done.

# what is the tricky part?

Khó khăn trong việc tìm hiểu lại các cấu trúc cây nhị phân và bị lỗi giới hạn quyền push code
trong gitlab mặc dù đã config ssh-rsa key cho account cấp quyền vào project.

# which parts that you already know, and not know?

Tất cả đã từng làm qua nhưng phần xử lí dữ liệu cây nhị phân thì ít tiếp xúc do chủ yếu xử lí
data dạng mảng.

# how do you rate the difficulty of the test?

7/10
